﻿using Styx.Helpers;
using Styx.Plugins.PluginClass;
using Styx.WoWInternals;
using System;

namespace Jumpy
{
    public class Jumpy : HBPlugin
    {
        private readonly WaitTimer sleeping = WaitTimer.ThirtySeconds;

        public override void Pulse()
        {
            if (sleeping.IsFinished && ObjectManager.Me.IsAlive && !ObjectManager.Me.IsFlying && ObjectManager.Me.Casting == 0 && !ObjectManager.Me.Looting && !ObjectManager.Me.IsResting)
            {
                Lua.DoString("JumpOrAscendStart();");
                Logging.Write("[Jumpy] We jumped!");
                sleeping.Reset();
            }
        }

        public override string Name { get { return "Jumpy"; } }
        public override string Author { get { return "Dreamful"; } }
        public override Version Version { get { return new Version(1, 0, 0, 0); } }
    }
}