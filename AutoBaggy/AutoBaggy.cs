﻿using Styx.Plugins.PluginClass;
using Styx.WoWInternals;
using System;

namespace AutoBaggy
{
    public class Bags : HBPlugin
    {
        public override void Pulse()
        {
            if (ObjectManager.Me.IsAlive && !ObjectManager.Me.Combat && ObjectManager.Me.IsResting)
            {
                Lua.DoString("RunMacroText(\"/equip Small Red Pouch\");");
                Lua.DoString("RunMacroText(\"/equip Small Blue Pouch\");");
                Lua.DoString("RunMacroText(\"/equip Small Brown Pouch\");");
                Lua.DoString("RunMacroText(\"/equip Old Moneybag\");");
                Lua.DoString("RunMacroText(\"/equip Small Black Pouch\");");
                Lua.DoString("RunMacroText(\"/equip Small Green Pouch\");");
                Lua.DoString("RunMacroText(\"/equip Blue Leather Bag\");");
                Lua.DoString("RunMacroText(\"/equip Red Leather Bag\");");
                Lua.DoString("RunMacroText(\"/equip Gnoll Hide Sack\");");
                Lua.DoString("RunMacroText(\"/equip Brown Leather Satchel\");");
                Lua.DoString("RunMacroText(\"/equip Green Leather Bag\");");
                Lua.DoString("RunMacroText(\"/equip White Leather Bag\");");
                Lua.DoString("RunMacroText(\"/equip Handmade Leather Bag\");");
                Lua.DoString("RunMacroText(\"/equip Large Blue Sack\");");
                Lua.DoString("RunMacroText(\"/equip Large Red Sack\");");
                Lua.DoString("RunMacroText(\"/equip Deviate Hide Pack\");");
                Lua.DoString("RunMacroText(\"/equip Fel Steed Saddlebags\");");
                Lua.DoString("RunMacroText(\"/equip Large Green Sack\");");
                Lua.DoString("RunMacroText(\"/equip Large Brown Sack\");");
                Lua.DoString("RunMacroText(\"/equip Large Knapsack\");");
                Lua.DoString("RunMacroText(\"/equip Huge Brown Sack\");");
                Lua.DoString("RunMacroText(\"/equip Mageweave Bag\");");
                Lua.DoString("RunMacroText(\"/equip Troll-hide Bag\");");
                Lua.DoString("RunMacroText(\"/equip Journeyman's Backpack\");");
                Lua.DoString("RunMacroText(\"/equip Traveler's Backpack\");");
            }
        }

        public override string Name { get { return "AutoBaggy"; } }
        public override string Author { get { return "Dreamful"; } }
        public override Version Version { get { return new Version(1, 0, 0, 0); } }
    }
}